#pragma once

#include <stdbool.h>
#include <stdint.h>

enum sprite_type_t {
  SNAKE,
  ENEMY,
  FRUIT,
  OBSTACLE,
};

enum direction_t {
  NONE,
  UP,
  DOWN,
  LEFT,
  RIGHT,
};

typedef struct SPRITE_BODY {
  uint8_t x;
  uint8_t y;
  struct SPRITE_BODY *next;
  struct SPRITE_BODY *prev;
  bool new_extension;
} body_t;

typedef struct {
  enum direction_t dir;
  uint8_t max_length;
  uint8_t speed;
  bool wrap_around; // Can the body go from one edge and appear on the opposite edge
  bool collidable;
  bool consumable;
  bool dead;
} attributes_t;

typedef struct {
  enum sprite_type_t type;
  attributes_t attr;
  body_t *head;
  body_t *tail;
  uint8_t r;
  uint8_t g;
  uint8_t b;
} sprite_t;

/**
 * Update the positions of each body part on each tick
 */
void sprite_step(sprite_t *sprite);

/**
 * Destroy the sprite
 */
void sprite_destroy(sprite_t *sprite);

/**
 * Destroy a body component of the sprite
 */
void sprite_body_destroy(body_t *body);

/**
 * Extend the tail of the sprite
 */
void sprite_extend(sprite_t *sprite);

/**
 * Chop off the tail of the sprite
 */
void sprite_shrink(sprite_t *sprite);

/**
 * Change direction the sprite will take on the next tick
 */
void sprite_direction_set(sprite_t *sprite, enum direction_t dir);

/**
 * Increase speed of sprite
 */
void sprite_speed_inc(sprite_t *sprite);

/**
 * Decrease speed of sprite
 */
void sprite_speed_dec(sprite_t *sprite);

/**
 * Handle sprite collision
 */
void sprite_collision(int x, int y, sprite_t *src_sprite, sprite_t *dst_sprite);