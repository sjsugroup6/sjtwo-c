#pragma once

#include "body.h"

#define ROWS 64
#define COLS 64

typedef struct SPRITE_STACK {
  sprite_t *data;
  struct SPRITE_STACK *next;
} sprite_stack_t;

typedef struct {
  bool vacant[ROWS][COLS];
  sprite_stack_t *sprite_stack;
} game_grid_t;

void game_step(game_grid_t *gg);