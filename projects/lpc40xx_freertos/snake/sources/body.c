#include <assert.h>
#include <stddef.h>
#include <stdlib.h>

#include "body.h"

void sprite_step(sprite_t *sprite) {
  /**
   * Assuming a snake of length 3, here are the minimalistic representations
   * for each group of bodies that are adjacent to each other in one direction.
   * (5,5) will be the head at T=0 in all the examples.
   *
   * Coordinate System
   *
   *  (63,0)  ..     ..    ..  (63,63)
   *   ..     ..     ..    ..    ..
   *  (1,0)   ..     ..    ..    ..
   *  (0,0)  (0,1)  (0,2)  ..  (0,63)
   *
   * Leftward + 0
   *             <- (5,5) <- (5,6) <- (5,7)
   * Leftward + 1
   *    <- (5,4) <- (5,5) <- (5,6)
   *
   * Rightward + 0
   *    (5,3) -> (5,4) -> (5,5) ->
   * Rightward + 1
   *             (5,4) -> (5,5) -> (5,6) ->
   *
   * Upward + 0     Upward + 1
   *                        ^
   *                      (6,5)
   *         ^              ^
   *       (5,5)          (5,5)
   *         ^              ^
   *       (4,5)          (4,5)
   *         ^
   *       (3,5)
   *
   * Downard + 0    Downward + 1
   *       (7,5)
   *         v
   *       (6,5)          (6,5)
   *         v              v
   *       (5,5)          (5,5)
   *         v              v
   *                      (4,5)
   *                        v
   */

  // TODO: When moving an obstacle type, all body parts must move in the same
  // direction unlike a snake. This variable is not being used yet.
  bool move_as_unit = sprite->type == OBSTACLE;

  uint8_t new_head_x = sprite->head->x;
  uint8_t new_head_y = sprite->head->y;
  body_t *cur = sprite->tail;
  body_t *prev = cur->prev;

  switch (sprite->attr.dir) {
  // TODO: What about wrap_around from edge to edge?
  case UP:
    new_head_x += sprite->attr.speed;
    break;
  case DOWN:
    new_head_x -= sprite->attr.speed;
    break;
  case LEFT:
    new_head_y -= sprite->attr.speed;
    break;
  case RIGHT:
    new_head_y += sprite->attr.speed;
    break;
  case NONE:
    // Our map module will eventually call this function with all sprites.
    // In case of non-moving obstacles or fruits, we want to do nothing.
    return;
  }

  // Walk backwards and fill in the tail's coordinates with its front neighbor
  while (prev) {
    if (prev->new_extension) {
      // When we call sprite_extend(), the new body will have the same coordinates
      // as the tail. After one tick, all body components will be 1 away from each other.
      prev->new_extension = false;
    } else {
      cur->x = prev->x;
      cur->y = prev->y;
    }
    cur = prev;
    prev = prev->prev;
  }

  // Finally update the coordinates of the head
  sprite->head->x = new_head_x;
  sprite->head->y = new_head_y;
}

void sprite_destroy(sprite_t *sprite) {
  body_t *walker = sprite->head;
  body_t *next = walker->next;

  while (walker) {
    next = walker->next;
    sprite_body_destroy(walker);
    walker = next;
  }
  free(sprite);
}

void sprite_body_destroy(body_t *body) { free(body); }

void sprite_extend(sprite_t *sprite) {
  body_t *cur_tail = sprite->tail;
  body_t *new_tail = malloc(sizeof(body_t));

  new_tail->x = cur_tail->x;
  new_tail->y = cur_tail->y;
  new_tail->new_extension = true;
  cur_tail->next = new_tail;
  sprite->tail = new_tail;
}

void sprite_shrink(sprite_t *sprite) {
  body_t *cur_tail = sprite->tail;
  body_t *new_tail = cur_tail->prev;

  if (!new_tail) {
    return; // Cannot shrink less than 1 unit
  }

  cur_tail->next = NULL;

  // We need to ensure that the map grid still has access to the coordinates
  // so that we can toggle off the LEDs for this sprite.
  sprite_destroy(sprite);
}

void sprite_direction_set(sprite_t *sprite, enum direction_t dir) { sprite->attr.dir = dir; }

void sprite_speed_inc(sprite_t *sprite) { sprite->attr.speed++; }

void sprite_speed_dec(sprite_t *sprite) { sprite->attr.speed--; }

void sprite_collision(int x, int y, sprite_t *src_sprite, sprite_t *dst_sprite) {
  assert(src_sprite->attr.collidable && dst_sprite->attr.collidable);

  // TODO
}
