#include "game.h"
#include "body.h"

void game_step(game_grid_t *gg) {
  sprite_stack_t *walker = gg->sprite_stack;

  while (walker) {
    sprite_step(walker->data);
    walker = walker->next;
  }
}